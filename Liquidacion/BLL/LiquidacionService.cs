﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
using BLL;
using DAL;
using System.Data.SqlClient;
using System.IO;


namespace BLL
{
    public class LiquidacionService
    {


        private SqlConnection conexion;
        private LiquidacionRepository liquidacionRepository;
        string CadenaConexion = @"Data Source=DESKTOP-NO6VVCF\SQLEXPRESS;Initial Catalog=Liquidacion;Integrated Security=True";

        public LiquidacionService()
        {
            conexion = new SqlConnection(CadenaConexion);

            liquidacionRepository = new LiquidacionRepository(conexion);
        }

        public bool CargarArchivo(string ruta, string codigoSucursal, int mes, int año)
        {
            try
            {
                conexion.Open();
                return liquidacionRepository.CargarArchivo(ruta, codigoSucursal, mes, año);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            finally
            {
                conexion.Close();
            }

        }


        public List<Liquidacion> ConsultarLiquidacion(int anio, int mes)
        {
            try
            {
                conexion.Open();
                return liquidacionRepository.ConsultarLiquidacion(anio, mes);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
            finally
            {
                conexion.Close();
            }
        }


        public decimal Liquidar(int anio, int mes)
        {
           


            try
            {
                conexion.Open();
                return liquidacionRepository.SumarLquidacion(anio, mes);
            }
            catch (Exception)
            {

                return 0;
            }
            finally
            {
                conexion.Close();
            }
        }


    }
}
