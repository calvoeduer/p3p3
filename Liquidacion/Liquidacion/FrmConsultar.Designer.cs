﻿namespace Liquidacion
{
    partial class FrmConsultar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TbConsulta = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.CmbMes = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtVigencia = new System.Windows.Forms.TextBox();
            this.BtnConsultar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtLiquidar = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.TbConsulta)).BeginInit();
            this.SuspendLayout();
            // 
            // TbConsulta
            // 
            this.TbConsulta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TbConsulta.Location = new System.Drawing.Point(54, 97);
            this.TbConsulta.Name = "TbConsulta";
            this.TbConsulta.Size = new System.Drawing.Size(735, 290);
            this.TbConsulta.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(278, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Vigencia";
            // 
            // CmbMes
            // 
            this.CmbMes.FormattingEnabled = true;
            this.CmbMes.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.CmbMes.Location = new System.Drawing.Point(90, 32);
            this.CmbMes.Name = "CmbMes";
            this.CmbMes.Size = new System.Drawing.Size(113, 21);
            this.CmbMes.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(86, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Mes";
            // 
            // TxtVigencia
            // 
            this.TxtVigencia.Location = new System.Drawing.Point(281, 33);
            this.TxtVigencia.Name = "TxtVigencia";
            this.TxtVigencia.Size = new System.Drawing.Size(113, 20);
            this.TxtVigencia.TabIndex = 6;
            // 
            // BtnConsultar
            // 
            this.BtnConsultar.Location = new System.Drawing.Point(570, 24);
            this.BtnConsultar.Name = "BtnConsultar";
            this.BtnConsultar.Size = new System.Drawing.Size(113, 35);
            this.BtnConsultar.TabIndex = 10;
            this.BtnConsultar.Text = "Consultar";
            this.BtnConsultar.UseVisualStyleBackColor = true;
            this.BtnConsultar.Click += new System.EventHandler(this.BtnConsultar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(121, 439);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Total a pagar";
            // 
            // TxtLiquidar
            // 
            this.TxtLiquidar.Location = new System.Drawing.Point(242, 439);
            this.TxtLiquidar.Name = "TxtLiquidar";
            this.TxtLiquidar.Size = new System.Drawing.Size(113, 20);
            this.TxtLiquidar.TabIndex = 11;
            // 
            // FrmConsultar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(873, 499);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtLiquidar);
            this.Controls.Add(this.BtnConsultar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.CmbMes);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtVigencia);
            this.Controls.Add(this.TbConsulta);
            this.Name = "FrmConsultar";
            this.Text = "FrmConsultar";
            ((System.ComponentModel.ISupportInitialize)(this.TbConsulta)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView TbConsulta;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CmbMes;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtVigencia;
        private System.Windows.Forms.Button BtnConsultar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtLiquidar;
    }
}