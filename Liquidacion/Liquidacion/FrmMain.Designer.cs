﻿namespace Liquidacion
{
    partial class FrmMain
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TxtAnio = new System.Windows.Forms.TextBox();
            this.CmbSede = new System.Windows.Forms.ComboBox();
            this.CmbMes = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnCargarArchivo = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre Sede";
            // 
            // TxtAnio
            // 
            this.TxtAnio.Location = new System.Drawing.Point(155, 177);
            this.TxtAnio.Name = "TxtAnio";
            this.TxtAnio.Size = new System.Drawing.Size(113, 20);
            this.TxtAnio.TabIndex = 1;
            // 
            // CmbSede
            // 
            this.CmbSede.FormattingEnabled = true;
            this.CmbSede.Items.AddRange(new object[] {
            "20001",
            "05001",
            "17001",
            ""});
            this.CmbSede.Location = new System.Drawing.Point(155, 65);
            this.CmbSede.Name = "CmbSede";
            this.CmbSede.Size = new System.Drawing.Size(113, 21);
            this.CmbSede.TabIndex = 2;
            // 
            // CmbMes
            // 
            this.CmbMes.FormattingEnabled = true;
            this.CmbMes.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.CmbMes.Location = new System.Drawing.Point(155, 119);
            this.CmbMes.Name = "CmbMes";
            this.CmbMes.Size = new System.Drawing.Size(113, 21);
            this.CmbMes.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(106, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Mes";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(107, 184);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Año";
            // 
            // BtnCargarArchivo
            // 
            this.BtnCargarArchivo.Location = new System.Drawing.Point(74, 276);
            this.BtnCargarArchivo.Name = "BtnCargarArchivo";
            this.BtnCargarArchivo.Size = new System.Drawing.Size(117, 55);
            this.BtnCargarArchivo.TabIndex = 6;
            this.BtnCargarArchivo.Text = "CARGAR";
            this.BtnCargarArchivo.UseVisualStyleBackColor = true;
            this.BtnCargarArchivo.Click += new System.EventHandler(this.BtnCargarArchivo_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(310, 276);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(106, 55);
            this.button2.TabIndex = 7;
            this.button2.Text = "CONSULTAR";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 450);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.BtnCargarArchivo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.CmbMes);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CmbSede);
            this.Controls.Add(this.TxtAnio);
            this.Controls.Add(this.label1);
            this.Name = "FrmMain";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtAnio;
        private System.Windows.Forms.ComboBox CmbSede;
        private System.Windows.Forms.ComboBox CmbMes;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BtnCargarArchivo;
        private System.Windows.Forms.Button button2;
    }
}

