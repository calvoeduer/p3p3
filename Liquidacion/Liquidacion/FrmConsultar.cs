﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using Entity;

namespace Liquidacion
{
    public partial class FrmConsultar : Form
    {

        LiquidacionService liquidacionService = new LiquidacionService();
        

        public FrmConsultar()
        {
            InitializeComponent();
        }

        private void BtnConsultar_Click(object sender, EventArgs e)
        {
            int anio = int.Parse(TxtVigencia.Text.Trim());
            int mes = int.Parse(CmbMes.SelectedItem.ToString().Trim());

     

            TbConsulta.DataSource = liquidacionService.ConsultarLiquidacion(anio, mes);

            TxtLiquidar.Text = liquidacionService.Liquidar(anio, mes).ToString();

            



        }
    }
}
