﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entity;
using BLL;


namespace Liquidacion
{
    public partial class FrmMain : Form
    {

        LiquidacionService liquidacionService = new LiquidacionService(); 

        public FrmMain()
        {
            InitializeComponent();
        }

        private void BtnCargarArchivo_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();

            if (open.ShowDialog() == DialogResult.OK)
            {
                string cadena = open.FileName;
                string codigoSede = CmbSede.SelectedItem.ToString();
                int mes = int.Parse( CmbMes.SelectedItem.ToString());
                int año = int.Parse(TxtAnio.Text);


                if (liquidacionService.CargarArchivo(cadena, codigoSede, mes, año))
                {
                    MessageBox.Show("Recaudos cargados correctamente");
                }

                else
                {
                    MessageBox.Show("Datos inconsistentes. Revise el archivo log.txt");
                }


            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmConsultar frmConsultar = new FrmConsultar();
            frmConsultar.Show();
        }
    }
}
