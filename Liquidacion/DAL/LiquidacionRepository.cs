﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Entity;

namespace DAL
{
    public class LiquidacionRepository
    {
        SqlConnection conexion;
        Liquidacion liquidacion;

        SqlDataReader datos;
        List<Liquidacion> liquidaciones;



        public LiquidacionRepository(SqlConnection sqlConnection)
        {
            this.conexion = sqlConnection;
        }

        //---------------------------------------------------------------------------------------

        public bool GuardarLog(Liquidacion liquidacion)
        {
            string ruta = @"Log.txt";

            FileStream origenFlujo = new FileStream(ruta, FileMode.Append);
            StreamWriter escritor = new StreamWriter(origenFlujo);
            escritor.WriteLine(liquidacion.CodigoSucursal + ";" + liquidacion.IdContratista + ";" + liquidacion.Nombre + ";" + liquidacion.NIC + ";" +
            liquidacion.Fecha.ToString("dd/MM/yyyy") + ";" + liquidacion.CodigoServicio + ";" + liquidacion.Valor);
            escritor.Close();
            origenFlujo.Close();

            return true;
        }

        public void GuardarLiquidacionDB(Liquidacion liquidacion)
        {
            using (var commando = conexion.CreateCommand())
            {
                commando.CommandText = "INSERT INTO TBLiquidacion(CodigoSucursal, IdContratista, Nombre, NIC, Fecha, CodigoServicio, Valor)  " +
                                       "VALUES(@codigoSucursal, @idContratista, @nombre, @nIC, @fecha, @codigoServicio, @valor)";


                commando.Parameters.AddWithValue("@codigoSucursal", liquidacion.CodigoSucursal);
                commando.Parameters.AddWithValue("@idContratista", liquidacion.IdContratista);
                commando.Parameters.AddWithValue("@nombre", liquidacion.Nombre);
                commando.Parameters.AddWithValue("@nIC", liquidacion.NIC);
                commando.Parameters.AddWithValue("@fecha", liquidacion.Fecha);
                commando.Parameters.AddWithValue("@codigoServicio", liquidacion.CodigoServicio);
                commando.Parameters.AddWithValue("@valor", liquidacion.Valor);
                commando.ExecuteNonQuery();
            }


        }

        public bool CargarArchivo(string ruta, string codigoSucursal, int mes, int año)
        {
            FileStream origen = new FileStream(ruta, FileMode.OpenOrCreate);
            StreamReader reader = new StreamReader(origen);
            string linea = string.Empty;
       

            while ((linea = reader.ReadLine()) != null)
            {
                Liquidacion liquidacion = Mapear(linea);

                if ((liquidacion.CodigoSucursal == codigoSucursal) && (liquidacion.Fecha.Month == mes) && (liquidacion.Fecha.Year == año) && Validar(liquidacion) )
                {
                    GuardarLiquidacionDB(liquidacion);

                }
                else
                {
                    GuardarLog(liquidacion);
                }

            }



            reader.Close();
            origen.Close();
            return true;
        }

        private bool Validar(Liquidacion liquidacion)
        {
            switch (liquidacion.CodigoServicio)
            {
                case "01":
                    if (liquidacion.Valor == 30000)
                    {
                        return true;

                    }
                    break;

                case "02":
                    if (liquidacion.Valor == 20000)
                    {
                        return true;

                    }
                    break;

                case "03":
                    if (liquidacion.Valor == 10000)
                    {
                        return true;

                    }

                    break;
            }

            return false;
        }

        private Liquidacion Mapear(string linea)
        {
            string[] datos = linea.Split(';');
            liquidacion = new Liquidacion();
            liquidacion.CodigoSucursal = datos[0];
            liquidacion.IdContratista = datos[1];
            liquidacion.Nombre = datos[2];
            liquidacion.NIC = datos[3];
            liquidacion.Fecha = DateTime.Parse(datos[4]);
            liquidacion.CodigoServicio = datos[5];
            liquidacion.Valor = decimal.Parse(datos[6]);
            return liquidacion;
        }





        /*´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´*/
        public List<Liquidacion> ConsultarLiquidacion(int anio, int mes)
        {
            List<Liquidacion> liquidacion = new List<Liquidacion>();

            using (var comando = conexion.CreateCommand())
            {

                /*CodigoSucursal, IdContratista, Nombre, NIC, Fecha, CodigoServicio, Valor*/
                comando.CommandText = "SELECT IdContratista, Nombre, YEAR(Fecha) Vigencia, MONTH(Fecha) Mes, SUM(Valor) Liquidacion " +
                    "FROM TBLiquidacion GROUP BY IdContratista, Nombre, YEAR(Fecha), MONTH(Fecha) " +
                    "HAVING YEAR(Fecha) = @anio AND MONTH(fecha) = @mes";

                comando.Parameters.AddWithValue("@anio", anio);
                comando.Parameters.AddWithValue("@mes", mes);

                SqlDataReader sqlDataReader = comando.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    liquidacion.Add(MapearRegistro(sqlDataReader));
                }
            }

            return liquidacion;
        }

        private Liquidacion MapearRegistro(SqlDataReader sqlDataReader)
        {
            Liquidacion liquidacion = new Liquidacion()
            {
               IdContratista  = sqlDataReader.GetString(0),
                Nombre = sqlDataReader.GetString(1),
                Fecha = DateTime.Parse($"01/{sqlDataReader.GetInt32(2)}/{sqlDataReader.GetInt32(3)}"),
                Valor = sqlDataReader.GetDecimal(4)
            };

            return liquidacion;
        }


        public decimal SumarLquidacion(int anio, int mes)
        {

            return ConsultarLiquidacion(anio, mes).Sum(p => p.Valor);
        }

    }
}
